/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bithole.pdflab.piechart;

import java.awt.Font;
import java.awt.image.BufferedImage;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.font.PDFont;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.apache.pdfbox.pdmodel.graphics.image.JPEGFactory;
import org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PiePlot;
import org.jfree.data.general.DefaultPieDataset;

/**
 *
 * @author yasitha_i
 */
public class PdfboxFreechartPiechart {
        public static void PdfboxFeeChartPie() {
                try {
            DefaultPieDataset dataset = new DefaultPieDataset();
            dataset.setValue("One", new Double(43.2));
            dataset.setValue("Two", new Double(10.0));
            dataset.setValue("Three", new Double(27.5));
            dataset.setValue("Four", new Double(17.5));
            dataset.setValue("Five", new Double(11.0));
            dataset.setValue("Six", new Double(19.4));

            JFreeChart chart = ChartFactory.createPieChart(
                    "Pie Chart Demo 1",  // chart title
                    dataset,             // data
                    true,               // include legend
                    true,
                    false
            );

            PiePlot plot = (PiePlot) chart.getPlot();
            plot.setLabelFont(new Font("SansSerif", Font.PLAIN, 12));
            plot.setNoDataMessage("No data available");
            plot.setCircular(false);
            plot.setLabelGap(0.02);

            BufferedImage chartImage = chart.createBufferedImage(300, 230);

            PDDocument document = new PDDocument();
            PDPage page = new PDPage();

            document.addPage(page);
            PDFont font = PDType1Font.HELVETICA_BOLD;

            PDPageContentStream content = new PDPageContentStream(document, page);
            content.beginText();
            content.setFont( font, 12 );
            content.newLineAtOffset(150, 750);
            content.showText("Hello to pdfbox and JFreeChart world");

            content.endText();

            PDImageXObject pdfChartImage = JPEGFactory.createFromImage(document, chartImage, 1f);
            content.drawImage(pdfChartImage, 50, 500);

            content.close();

            document.save("C://pdfboxfreechartpiechart.pdf");
            document.close();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
