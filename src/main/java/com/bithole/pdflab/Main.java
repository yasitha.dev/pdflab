/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bithole.pdflab;

import com.bithole.pdflab.piechart.ItextFreechart;
import com.bithole.pdflab.piechart.PdfboxFreechartPiechart;
import com.bithole.pdflab.piechart.PdfboxPieChart;
import java.io.IOException;

/**
 *
 * @author yasitha_i
 */
public class Main 
{
    public static void main(String[] args) throws IOException
    {
        ItextFreechart.itextpie();
        PdfboxPieChart.pdfboxPie();
        PdfboxFreechartPiechart.PdfboxFeeChartPie();
    }
}
